Quando('eu faço login com {string} e {string}') do |email, senha|
    @login_page.go 
    @login_page.with(email,senha)
end
  
Então('devo ser autenticado') do
    # Verificando se está atutenticado pelo token de acesso da página à API
    # No chrome vai em opções > mais ferramentas > ferramentas do desenvolvedor
    # Vai na aba network, limpa o histórico e faz o login
    # Na variável de name = auth conseguimos ver o response da API
    # Clicando na aba Application e no menu esquerdo indo em Storage > Local Storage conseguimos ver onde
    #   está guardado o user_id e o token (default_auth_token)
    # Com isso, conseguimos utilizar JScript para pegar este token.
    # No console do browser funciona da seguinte maneira
    # window.localStorage.getItem("NOME_DO_TOKEN_QUE_ESTÁ_NO_STORAGE");
    # Aqui, precisaremos do 'return' para conseguir pegar o valor!
    # js_script = 'return window.localStorage.getItem("default_auth_token");'
    # token = page.execute_script(js_script)
    # log(token) #Log é o antigo puts
    # Como a aplicação sempre devolve um token válido com 147 caracteres, verifico isso para saber se
    #   o login foi realizado com sucesso
    expect(get_token.length).to be 147
    # get_token está no modulo construido no arquivo helpers.rb e instanciado no env.rb

end
  
Então('devo ver {string} na área logada') do |expect_name|
    expect(@sidebar.logged_user).to eql expect_name
end

Então('não devo ser autenticado') do
    expect(get_token).to be nil
end
  
Então('devo ver a mensagem de alerta {string}') do |expect_message|
    expect(@login_page.alert).to eql expect_message
end