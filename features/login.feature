#language: pt

Funcionalidade: Login
    Para que eu possa gerenciar os filmes do catálogo NinjaFlix
    Sendo um usuário previamente cadastradado
    Posso acessar o sistema com o meu email e senha

    @log_acesso
    Cenário: Acesso
        Quando eu faço login com "tony@stark.com" e "pwd123"
        Então devo ser autenticado
        E devo ver "Tony Stark" na área logada

    @log
    Esquema do Cenário: Login sem sucesso
    Quando eu faço login com <email> e <senha>
    Então não devo ser autenticado
    E devo ver a mensagem de alerta <mensagem>
    
    Exemplos:
        | email            | senha     | mensagem                       | 
        | "tony@stark.com" | "abc123"  | "Usuário e/ou senha inválidos" | 
        | "404@yahoo.com"  | "abc123"  | "Usuário e/ou senha inválidos" | 
        | ""               | "abcxpto" | "Opps. Cadê o email?"          | 
        | "test@email.com" | ""        | "Opps. Cadê a senha?"          |