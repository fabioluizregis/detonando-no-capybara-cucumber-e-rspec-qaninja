require 'report_builder'

Before do
    @login_page = LoginPage.new
    @movie_page = MoviePage.new
    @sidebar = SidebarView.new 

    # page.driver.browser.manage.window.maximize
    page.current_window.resize_to(1440,900) #resolução mínima do projeto
end

Before("@login") do
    user = CONFIG["users"]["cat_manager"]
    @login_page.go
    @login_page.with(user["email"], user["pass"])
end

After do |scenario|
    # if scenario.failed? #tira evidencia só quando falha
        temp_shot = page.save_screenshot("log/temp_shot.png")
        screenshot = Base64.encode64(File.open(temp_shot, "rb").read)
        #Guardando a imagem no relatório, formato da imagem, Nome do link no relatório
        embed(screenshot, "image/png", "Screenshot")
    # end
end

d = DateTime.now
@current_date = d.to_s.tr(":","-") # substituindo o : da hora por -
at_exit do
    ReportBuilder.configure do |config|
    config.input_path = "log/report.json"
    config.report_path = "log/" + @current_date
    config.report_types = [:html]
    config.report_title = "Ninjaflix - WebApp"
    config.compress_image = true
    config.additional_info = {"App" => "Web", "Data de Execução" => @current_date}
    config.color = "indigo"
    end
    ReportBuilder.build_report
end