require 'capybara'
require 'capybara/cucumber'
require 'selenium-webdriver'
require "os"                    #resolver problemas de Sistemas Operacionais diferentes

require_relative "helpers"

World(Helpers)

CONFIG = YAML.load_file(File.join(Dir.pwd, "features/support/config/#{ENV["ENV_TYPE"]}.yaml"))

case ENV["BROWSER"]
    when "firefox"
        @driver = :selenium
    when "chrome"
        @driver = :selenium_chrome
    when "headless"
        @driver = :selenium_chrome_headless
    else
        log("Invalid Browser!")
end

Capybara.configure do |config|
    config.default_driver = @driver
    config.app_host =  CONFIG["url"] # Docker Toolbox no windows

    config.default_max_wait_time = 10
end